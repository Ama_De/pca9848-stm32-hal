#include "PCA9848.h"

void PCA9848_init (PCA9848_HandleTypeDef *i2c_multiplexer, I2C_HandleTypeDef *i2c, uint16_t address) {
	i2c_multiplexer->i2cInstantion = i2c;
	i2c_multiplexer->i2cAddress = address;
}

void PCA9848_softwareReset (PCA9848_HandleTypeDef *i2c_multiplexer) {
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(i2c_multiplexer->i2cInstantion, 0b00000000, 0, 1, (uint8_t*)0x06, 1, PCA9848_I2C_TIMEOUT);
}

HAL_StatusTypeDef PCA9848_setChannels (PCA9848_HandleTypeDef *i2c_multiplexer, uint8_t *controlRegister) {
	if (*controlRegister >= 0 && *controlRegister <= 255) {
		return HAL_I2C_Mem_Write(i2c_multiplexer->i2cInstantion, i2c_multiplexer->i2cAddress, 0, 1, controlRegister, 1, PCA9848_I2C_TIMEOUT);
	}
	return HAL_ERROR;
}

HAL_StatusTypeDef PCA9848_readControlRegister (PCA9848_HandleTypeDef *i2c_multiplexer, uint8_t *controlRegister) {
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(i2c_multiplexer->i2cInstantion, i2c_multiplexer->i2cAddress, 0, 1, controlRegister, 1, PCA9848_I2C_TIMEOUT);
	return status;
}
