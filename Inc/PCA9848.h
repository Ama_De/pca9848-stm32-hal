/*
 PCA9848.h - Library for handling PCA9848 I2C 8 to 1 multiplexer over I2C bus.
 Created by Amadeusz Świerk, 27.04.2021
 Released into the public domain, MIT license.
 */

#ifndef PCA9848_h
#define PCA9848_h

#include "stm32f0xx_hal.h"
//#include "stdio.h"

#define PCA9848_I2C_TIMEOUT 1

// channels masks
#define PCA9848_channel1_disable 0x7F
#define PCA9848_channel1_enable 0x80

#define PCA9848_channel2_disable 0xBF
#define PCA9848_channel2_enable 0x40

#define PCA9848_channel3_disable 0xDF
#define PCA9848_channel3_enable 0x20

#define PCA9848_channel4_disable 0xEF
#define PCA9848_channel4_enable 0x10

#define PCA9848_channel5_disable 0xF7
#define PCA9848_channel5_enable 0x08

#define PCA9848_channel6_disable 0xFB
#define PCA9848_channel6_enable 0x04

#define PCA9848_channel7_disable 0xFD
#define PCA9848_channel7_enable 0x02

#define PCA9848_channel8_disable 0xFE
#define PCA9848_channel8_enable 0x01

#define PCA9848_msbMask 0b00001111

typedef struct PCA9848 {
	I2C_HandleTypeDef *i2cInstantion;
	uint16_t i2cAddress;
} PCA9848_HandleTypeDef;

void PCA9848_init(PCA9848_HandleTypeDef *i2c_multiplexer, I2C_HandleTypeDef *i2c, uint16_t address);
void PCA9848_softwareReset(PCA9848_HandleTypeDef *i2c_multiplexer);

HAL_StatusTypeDef PCA9848_setChannels(PCA9848_HandleTypeDef *i2c_multiplexer, uint8_t *controlRegister);
HAL_StatusTypeDef PCA9848_readControlRegister(PCA9848_HandleTypeDef *i2c_multiplexer, uint8_t *controlRegister);

#endif
