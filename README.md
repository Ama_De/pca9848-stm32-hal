# PCA9848 STM32 HAL I2C library

Library for handling PCA9848 I2C 8 to 1 multiplexer over I2C bus.
    
GNU License  
Copyright (c) 2021, Amadeusz Świerk